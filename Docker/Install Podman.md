The podman package is available in the Debian 11 (Bullseye) repositories and later.
```
sudo apt-get -y install podman
```