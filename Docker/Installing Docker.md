1. Log into your Linux machine, and download the required script with the command _
```bash curl -fsSL_ _https://get.docker.com_ _-o install-docker.sh```

2. Next, give the new script executable permissions with 
```bash chmod u+x install-docker.sh```

3. Run the script with the command 
```bash sudo ./install-docker.sh```
This will install all of the necessary dependencies as well as the Docker runtime engine on your machine.

4. The last thing you must do is add your user to the Docker group with the command 
```bash sudo usermod -aG docker $USER```
