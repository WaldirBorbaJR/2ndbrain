Em termos de **padrões e nomenclaturas no [[Git]]**. É sempre sagaz ter um padrão de nomenclaturas e um nome explicativo nas branches.

- bugfix/
- feature/
- hotfix/
- improvement/
#### bugfix/

Como o próprio nome já diz, é um BUG e precisa ser corrigido de forma imediata, o quanto antes. Num outro artigo eu explico melhor a utilização desse cara e _branches principais_.

#### feature/

O nome já diz também o que é, uma nova feature que será adicionada ao projeto, componente e afins.

#### hotfix/

Às vezes esse termo pode ser usado de outras formas, até mesmo para usar no lugar do bugfix. Porém, eu prefiro separar, deixar com semânticas diferentes.

Ele é bem similar ao bugfix/, porém, ele não é um BUG, mas sim uma correção, seja ela de cor, textos, alterações não tão urgentes, que não signifiquem BUG's.

#### improvement/

O nome já mostra para o que serve. Em si é uma melhoria para um fodasse já existente, seja de performance, de escrita, de layout, etc.