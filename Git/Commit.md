Em termos de padrões e nomenclaturas no [[Git]], é essencial, pois da para analisar um PR olhando as mensagens de commit. Se elas forem bem explicativas, já é possivel saber o porquê daquele PR.

- **feat:** que serve para novas features que você adicionar
- **fix:** o nome já diz, serve para você corrigir algum fodasse lá
- **docs:** mais um fácil, para algo relacionado a documentações, README e afins
- **style:** mexeu no estilo, CSS? Manda brasa então nesse cara
- **refactor:** precisou alterar, melhorar algum fodasse? É esse caboclo aqui
- **perf:** quando você mexer em algo relacionado a performance, fique à vontade em usar esse aqui
- **test:** para testes, ok?
- **build:** mais específicos para tarefas de build
- **ci:** para algo que você mexa na Integração Contínua dele
- **revert:** Finalmente algum para eu explicar. O nome já diz o que é, certo? Caso você precise fazer um revert, manda ver nesse prefixo
- **chore:** geralmente o mais emblemático. Serve para coisas relacionados a build, configs e afins. Por exemplo, mexeu em algo no package.json? Use esse cara, seja atualizando a versão do pacote ou instalando novas dependências

![[commitizen.png]]