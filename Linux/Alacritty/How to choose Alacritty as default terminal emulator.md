
This is not necessarily specified in the documentation, but to launch [Alacritty](https://github.com/jwilm/alacritty) with the command to open its default terminal, it is necessary to define it as an alternative available for ‘terminal’ type applications:

```
sudo update-alternatives --install /usr/bin/x-terminal-emulator x-terminal-emulator /usr/bin/alacritty 50
```

```
sudo update-alternatives --install /usr/bin/x-terminal-emulator x-terminal-emulator /home/borba/bin/wezterm 50
```

(The number ‘50’ at the end represents the priority of the entry)

It will then be possible to define it as the default terminal, thanks to the command:

```
sudo update-alternatives --config x-terminal-emulator
```

Finally, if you want to remove alacritty from the available alternatives:

```
sudo update-alternatives --remove "x-terminal-emulator" "/usr/bin/alacritty"
```