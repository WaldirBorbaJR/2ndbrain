Some useful commands to execute [[multipass]]


```
multipass launch 22.04 --name ubuntu-k8s
```

```
multipass launch -n k8s -c 1 -m 2G -d 10G
``` 

```
multipass list
```

```
multipass find
```

```
multipass exec image_name -- lsb_release -a
``` 

```
multipass shell image_name
``` 

```
multipass stop image_name
```

```
multipass delete image_name
```

```
multipass purge
```

