# Ubuntu VMs on demand for any workstation

Get an instant Ubuntu VM with a single command. [[multipass]] can launch and run virtual machines and configure them with [cloud-init](https://cloud-init.io/) like a public cloud.